import java.util.*;
import java.util.regex.Pattern;

import javax.print.DocFlavor.STRING;

import java.util.regex.Matcher;
import java.text.SimpleDateFormat;
import java.io.*;

public class Assignment2Serverlog{

    public static void main(String[] args) {

        String [] variables={"date","Time","sender"};
        
       // SimpleDateFormat date= new SimpleDateFormat("YY/MM/DD");
        String commandstatus;

         //order of date,time,sender
        // String [] myregex ={"(^\\d{4}-\\d{2}-\\d{2})","(\\d{2}:\\d{2}:\\d{2})","[smsc^\\d+]:\\sSending"};                                           
        
        String dateRegex="(^\\d{4}-\\d{2}-\\d{2})";
        String timeRegex="(\\d{2}:\\d{2}:\\d{2})";
        String senderRegex="(smsc_\\w+)";
        
        try{
            String line;
        FileReader bearerfile= new FileReader("try.txt");//bearerbox_server.log
        BufferedReader reader= new BufferedReader(bearerfile);
       while((line=reader.readLine())!= null){
      
      //seperate the parts  of logs in line using split  
        String [] splitparts = line.split(": ");

     
    for(String item : splitparts){
        item.trim();
       // System.out.println(item);
           matching(dateRegex,item,variables[0]);
           matching(timeRegex,item, variables[1]);
           matching(senderRegex,item,variables[2]);

       }
       }   
    
        }
    catch(Exception e){
        return;
    }
}
public static void matching(String regex,String input,String n){
             Map<String,String>map = new HashMap<String,String>();
  Pattern pattern = Pattern.compile(regex)  ;
  Matcher  matcher= pattern.matcher(input);
        
  
          if (matcher.find()){
 
             map.put(n, matcher.group());
        for(Map.Entry m:map.entrySet()){
        
      System.out.println(m.getKey()+"  "+ m.getValue());
        }
}
}

}   

